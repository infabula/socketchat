var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var users = [];
var connections = [];
var port = 3000;
server.listen(port);
console.log("Server running on port " + port);

// serve static content
app.use(express.static(__dirname + '/static'));

// middleware parsers for content
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('views', __dirname + 'views');
